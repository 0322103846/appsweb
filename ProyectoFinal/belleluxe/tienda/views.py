from django.shortcuts import render
from django.views import generic
from core.models import Product, Category
# Create your views here.

# compras ---------------------------------------------------------


class tienda(generic.View):
    template_name="tienda/tienda.html"
    context={}
    def get(self, request, *args, **kwargs):
        category = Category.objects.all() 
        products = Product.objects.all()
        self.context = {
            "products": products,
            "category":category

        }
        return render(request, self.template_name, self.context)
    
# Lista de productos ---------------------------------------------------------

class hombre(generic.View):
    template_name="tienda/hombre.html"
    context={}
    def get(self, request, *args, **kwargs):
        products = Product.objects.filter(gender='Hombre')
        self.context = {
            "products": products
        }
        return render(request, self.template_name, self.context)
    
class mujer(generic.View):
    template_name="tienda/mujer.html"
    context={}
    def get(self, request, *args, **kwargs):
        products = Product.objects.filter(gender='Mujer')
        self.context = {
            "products": products
        }
        return render(request, self.template_name, self.context)

# Productos ---------------------------------------------------------

class producto(generic.DetailView):
    template_name="tienda/producto.html"
    context={}
    def get(self, request, pk, *args, **kwargs):
        product=Product.objects.get(pk=pk)
        # pase la consulta aqui para poder crear otra variable y en ella gurdar una lista de nuemros
        # no se porque el djago no permite numeros y afuersas te piden cosas que no sean numeros como lisras
        #                A                  B
        opciones = range(1, product.stock + 1)
        # esta funcion te devuelve una lista del A hasta UNO ANTES QUE B
        self.context = {
            "product": product,
            "opciones":opciones
        
        }
        return render(request, self.template_name,self.context)
    def post(self, request):
        pass

    
class Pcategoria(generic.View):
   template_name = "tienda/tienda.html"
   context={}
   def get(self, request, pk, *args, **kwargs):
        categorypk = Category.objects.get(pk=pk)
        category = Category.objects.all() 
        products = Product.objects.filter(category=categorypk)
        self.context = {
            "products": products,
            'category':category
        }
        return render(request, self.template_name, self.context)


   
class busqueda(generic.View):
   template_name = "tienda/tienda.html"
   context={}
   def get(self, request, *args, **kwargs):
        slug = 'vestido'
        products = Product.objects.filter(slug=slug)
        self.context = {
            
            "products": products
        }
        return render(request, self.template_name, self.context)

 
