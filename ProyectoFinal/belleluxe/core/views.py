from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import *
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator


# Esta cosa devuelve un buliano sobre si es staff o no
def bloqueador(user):
    return user.is_staff

#Ese decorador verificar el buliano
#El dispatch se encarga de asegurar que sea el decorador lo primero en la vissta 
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class adm(generic.View):
    tamplate_name="core/admin.html"
    context={}
    def get(self, request, *args, **kwargs):
        return render(request, self.tamplate_name, self.context)


#Campaign--------------------------------------------------------------------------
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class Campaign(generic.View):
   template_name="core/campaign/campaign.html"
   context={}
   def get(self,request):
       self.context= {
           "campaign": campaign.objects.all()
       }
       return render(request, self.template_name, self.context)


@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')  
class DetailCampaign(generic.DetailView):
   template_name = "core/campaign/detail_campaign.html"
   model = campaign

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class NewCampaign(generic.CreateView):
   template_name="core/campaign/NewCampaign.html"
   model = campaign
   form_class = NewCampaignform
   success_url = reverse_lazy("core:campaign")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class UpdateCampaign(generic.UpdateView):
   template_name="core/campaign/update_campaign.html"
   model = campaign
   form_class = UpdateCampaignForm
   success_url = reverse_lazy("core:campaign")


@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DeleteCampaign(generic.DeleteView):
   template_name="core/campaign/delete_campaign.html"
   model = campaign
   success_url = reverse_lazy("core:campaign")


#Category--------------------------------------------------------------------------
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class list_category(generic.View):
   template_name="core/category/category.html"
   context={}
   def get(self,request):
      categories = Category.objects.all()
      self.context = {
         "category": categories
      }
      return render(request, self.template_name, self.context)

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class NewCategory(generic.CreateView):
   template_name="core/category/New_category.html"
   model = Category
   form_class = NewCategoryForm
   success_url = reverse_lazy("core:category")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DetailCategory(generic.DetailView):
   template_name = "core/category/detail_category.html"
   model = Category
   def get(self, request, pk, *args, **kwargs):
      categories = Category.objects.get(pk=pk)
      self.context = {
         "category": categories
      }
      return render(request, self.template_name,self.context)
   
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class UpdateCategory(generic.UpdateView):
   template_name="core/category/update_category.html"
   model = Category
   form_class = UpdateCategoryForm
   success_url = reverse_lazy("core:category")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DeleteCategory(generic.DeleteView):
   template_name="core/category/delete_category.html"
   model = Category
   success_url = reverse_lazy("core:category")

#Product--------------------------------------------------------------------------
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class product(generic.View):
   template_name = "core/product/product.html"
   context = {}
   def get(self, request, *args, **kwargs):
      products = Product.objects.all()
      self.context = {
         "products": products
      }
      return render(request, self.template_name, self.context)
   
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class NewProduct(generic.CreateView):
   template_name = "core/product/new_product.html"
   model = Product
   form_class = NewProductForm
   success_url = reverse_lazy("core:product")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class RecoverProduct(generic.View):
   template_name = "core/product/recover_product.html"
   context = {}

   def get(self, request, pk, *args, **kwargs):
      self.context = {
         "product": Product.objects.get(pk=pk)
      }
      return render(request, self.template_name,self.context)

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class UpdateProduct(generic.UpdateView):
   template_name = "core/product/update_product.html"
   model = Product
   form_class = UpdateProductForm
   success_url = reverse_lazy("core:product")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DeleteProduct(generic.DeleteView):
   template_name = "core/product/delete_product.html"
   model = Product
   success_url = reverse_lazy("core:product")


#Customer-----------------------------------------------------------------------
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class list_customer(generic.View):
   template_name="core/customer/customer.html"
   context={}
   def get(self,request):
       self.context= {
           "customer": Customer.objects.all()
       }
       return render(request, self.template_name, self.context)
   
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DetailCustomer(generic.DetailView):
   template_name = "core/customer/detail_customer.html"
   model = Customer

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class UpdateCustomer(generic.UpdateView):
   template_name="core/customer/update_customer.html"
   model = Customer
   form_class = UpdateCustomerForm
   success_url = reverse_lazy("core:customer")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DeleteCustomer(generic.DeleteView):
   template_name="core/customer/delete_customer.html"
   model = Customer
   success_url = reverse_lazy("core:customer")

#ItemPedido-----------------------------------------------------------------------

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class list_ItemPedido(generic.View):
   template_name = "core/itempedido/Itempedido.html"
   context = {}
   def get(self, request, *args, **kwargs):
      ItemPedidos = ItemPedido.objects.all()
      self.context = {
         "ItemPedido": ItemPedidos
      }
      return render (request, self.template_name, self.context)
   
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class RecoverItemPedido(generic.View):
   template_name = "core/itempedido/recover_itempedido.html"
   context = {}
   def get(self, request, pk, *args, **kwargs):
      self.context = {
         "ItemPedido" : ItemPedido.objects.get(pk=pk)
      }
      return render(request, self.template_name, self.context)

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class UpdateItemPedido(generic.UpdateView):
   template_name = "core/itempedido/update_itempedido.html"
   model = ItemPedido
   form_class = UpdateItemPedidoForm
   success_url = reverse_lazy ("core:Itempedido")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class DeteleItemPedido(generic.DeleteView):
   template_name = "core/itempedido/delete_itempedido.html"
   model = ItemPedido
   success_url = reverse_lazy ("core:Itempedido")

   

#Pedido-----------------------------------------------------------------------

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class list_pedidos(generic.View):
   template_name="core/pedido/pedido.html"
   context={}
   def get(self,request):
      Pedidos = Pedido.objects.all()
      self.context= {
        "pedidos": Pedidos
      }
      return render(request, self.template_name, self.context)
   


   
@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class Detailpedidos(generic.View):
   template_name = "core/pedido/detail_pedido.html"
   context={}
   def get(self, request, pk, *args, **kwargs):
      ped = Pedido.objects.get(pk=pk)
      self.context = {
         "ped" : ped
      }
      return render(request, self.template_name, self.context)
   




@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class Updatepedidos(generic.UpdateView):
   template_name="core/pedido/update_pedido.html"
   model = Pedido
   form_class = UpdatepedidosForm
   success_url = reverse_lazy("core:pedido")

@method_decorator(user_passes_test(bloqueador, login_url='/'), name='dispatch')
class Deletepedidos(generic.DeleteView):
   template_name="core/pedido/delete_pedido.html"
   model = Pedido
   success_url = reverse_lazy("core:pedido")