from django import forms
from .models import *

#campaign----------------------------------------------------------------------------

class NewCampaignform(forms.ModelForm):
   class Meta:
       model = campaign
       fields = [
           "lasteditor",
           "name"
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }


class UpdateCampaignForm(forms.ModelForm):
   class Meta:
       model = campaign
       fields = [
           "lasteditor",
           "name"
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }

#category----------------------------------------------------------------------------
class NewCategoryForm(forms.ModelForm):
   class Meta:
       model = Category
       fields = [
           "lasteditor",
           "name",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la categoría"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }

class UpdateCategoryForm(forms.ModelForm):
   class Meta:
       model = Category
       fields = [
           "lasteditor",
           "name",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la categoría"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }

#product----------------------------------------------------------------------------
class NewProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            "lasteditor",
            "name",
            "campaign",
            "gender",
            "category",
            "description",
            "size",
            "price",
            "stock",
            "slug",
            "imagen1",
            "status",
        ] 

        widgets = {
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
            "name": forms.TextInput(attrs={ "type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "campaign": forms.Select(attrs={"class":"form-select form-control"}),
            "gender": forms.Select(attrs={"class":"form-select form-control"}),
            "category": forms.Select(attrs={"class":"form-select form-control"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows": 4, "placeholder":"Escribe la descripcion del Producto"}),
            "size": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la talla del Producto"}),
            "price" : forms.TextInput(attrs={"type":"number", "class":"form-control", "step":"any"}),
            "stock": forms.TextInput(attrs={"type":"number", "class":"form-control"}),
            "slug": forms.TextInput(attrs={ "type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "imagen1": forms.FileInput(attrs={ "type":"file", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
        }
        labels = {
        "lasteditor" : "Ultima persona que lo edito",
        "name": "Nombre",
        "imagen1": "Imagen 1",
        "campaign": "Campaña",
        "gender": "Genero",
        "category": "Categoria",
        "description": "Descripción",
        "size":"Talla",
        "price":"Precio unitario",
        "stock":"Cantidad",
        "slug":"Slug",
        "status":"Status",
        
    }
    


class UpdateProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            "lasteditor",
            "name",
            "campaign",
            "gender",
            "category",
            "description",
            "size",
            "price",
            "stock",
            "slug",
            "imagen1",
            "status",
        ] 

        widgets = {
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
            "name": forms.TextInput(attrs={ "type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "campaign": forms.Select(attrs={"class":"form-select form-control"}),
            "gender": forms.Select(attrs={"class":"form-select form-control"}),
            "category": forms.Select(attrs={"class":"form-select form-control"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows": 4, "placeholder":"Escribe la descripcion del Producto"}),
            "size": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la talla del Producto"}),
            "price" : forms.TextInput(attrs={"type":"number", "class":"form-control", "step":"any"}),
            "stock": forms.TextInput(attrs={"type":"number", "class":"form-control"}),
            "slug": forms.TextInput(attrs={ "type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "imagen1": forms.FileInput(attrs={ "type":"file", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            
        }
        labels = {
        "lasteditor" : "Ultima persona que lo edito",
        "name": "Nombre",
        "imagen1": "Imagen 1",
        "campaign": "Campaña",
        "gender": "Genero",
        "category": "Categoria",
        "description": "Descripción",
        "size":"Talla",
        "price":"Precio unitario",
        "stock":"Cantidad",
        "slug":"Slug",
        "status":"Status",
        
    }


        
#Customer-------------------------------------------------------------------------
        
class UpdateCustomerForm (forms.ModelForm):
    class Meta:
        model = Customer
        fields = [ 
            "user",
            "name_first",
            "last_name_pat",
            "last_name_mat",
            "phone",
            "cp",
            "streat",
            "number_home",
        ]
        widgets = {
            "user": forms.Select(attrs={"class":"form-select form-control"}),
            "name_first":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del cliente"}) ,
            "last_name_pat": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Apellido paterno del cliente"}),
            "last_name_mat": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Apellido materno del cliente"}),
            "phone":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Número de telefono"}),
            "cp":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Código postal"}),
            "streat":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Dirección del cliente"}),
            "number_home":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Número de casa"})
        }
        labels = {
            "user" : "Usuario",
            "name_first": "Nombre",
            "last_name_pat": "Primer apellido",
            "last_name_mat": "Segundo apellido",
            "phone": "Telefono",
            "cp": "Codigo postal",
            "streat": "Calle",
            "number_home": "Numero de casa",
        }
        

#ItemPedido-----------------------------------------------------------------------


class UpdateItemPedidoForm(forms.ModelForm):
    class Meta:
        model = ItemPedido
        fields = '__all__'
    
        widgets = {
            "pedido" : forms.Select(attrs={"class":"form-select form-control"}),
            "product" : forms.Select(attrs={"class":"form-select form-control"}),
            "cantidad" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Cantidad de producto"}),
            "codigo_producto" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Código del producto"})
        }

class UpdatepedidosForm(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = '__all__'
        exclude= ['productos']
        
        widgets = {
            "usuario" : forms.Select(attrs={"class":"form-select form-control"}),
            #"productos" : forms.Select(attrs={"class":"form-select form-control"}),
           #### "fecha" : forms.DateField(attrs={"class":}), 
            "codigo_pedido" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Código del pedido"})
        }

        
