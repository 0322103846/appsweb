from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from core.models import Customer
from django.forms.models import inlineformset_factory


class inseForm(forms.Form):
    username = forms.CharField(label=' ', widget=forms.TextInput(attrs={"type":"text", "class":"form-control card-text","placeholder":"Escribe tu nombre de usuario"}))
    password = forms.CharField(label=' ', widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control card-text","placeholder":"Escribe tu contraseña"}))

class RegForm(UserCreationForm):
    username = forms.CharField(max_length=32, label='Nombre de usuario', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu nombre de usuario"}))
    password1= forms.CharField(max_length=32, label='Contraseña', widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu contraseña"}))
    password2 = forms.CharField(max_length=32, label='Confirmar contraseña', widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Confirma tu contraseña"}))
    name_first = forms.CharField(max_length=35, label='Nombre', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu nombre"}))
    last_name_pat = forms.CharField(max_length=25, label='Primer apellido', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu primer apellido"}))
    last_name_mat = forms.CharField(max_length=25,required=False, label='Segundo apellido (si no tiene dejar en blanco)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu segundo apellido"}))
    phone = forms.CharField(max_length=15, label='Número de teléfono', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu numero de telefono"}))
    cp = forms.CharField(max_length=5, label='Código postal (Belle dluxe solo hace envios en Tijuana)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu código postal"}))
    streat = forms.CharField(max_length=25, label='Calle (Belle dluxe solo hace envios en Tijuana)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de tu calle"}))
    number_home = forms.CharField(max_length=5, label='Número exterior de casa (Belle dluxe solo hace envios en Tijuana)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el número exterior de tu casa"}))
    class Meta:
        model=User
        fields= [
            'username',
            'password1',
            'password2',
            'name_first',
            'last_name_pat',
            'last_name_mat',
            'phone',
            'cp',
            'streat',
            'number_home'
        ] 




class ActualizarUs(forms.ModelForm):
    class Meta:
        model = Customer
        fields= [
            'name_first',
            'last_name_pat',
            'last_name_mat',
            'phone',
            'cp',
            'streat',
            'number_home'
        ] 
        widgets = {
            "name_first": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu nombre"}),
            'last_name_pat': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Primer apellido"}),
            'last_name_mat': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Escribe tu segundo apellido"}),
            'phone': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu numero de telefono"}),
            'cp': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu codigo postal"}),
            'streat': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de tu calle"}),
            'number_home': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el numero exterior de tu casa"})
        }
        labels = {
            'name_first':'nombre',
            'last_name_pat':'Primer apellido',
            'last_name_mat':'Segundo apellido (si no tiene dejar en blanco)',
            'phone':'Número de teléfono',
            'cp':'Código postal (Belle dluxe solo hace envios en tijuana)',
            'streat':'Calle (Belle dluxe solo hace envios en tijuana)',
            'number_home':'Número exterior de casa (Belle dluxe solo hace envios en tijuana)'
        }






"""class RegForm(UserCreationForm):
    username = forms.CharField(max_length=32, label='Nombre de usuario', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}))
    password1= forms.CharField(max_length=32, label='Contraseña', widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu contraseña"}))
    password2 = forms.CharField(max_length=32, label='Confirmar contraseña', widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Confirma tu contraseña"}))
    class Meta:
        model=User
        fields= [
            'username',
            'password1',
            'password2',
        ] 

class Reg2Forms(forms.ModelForm):
    name_first = forms.CharField(max_length=32, label='Nombre', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu nombre"}))
    last_name_pat = forms.CharField(max_length=32, label='Primer apellido', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu primer apellido"}))
    last_name_mat = forms.CharField(max_length=32,required=False, label='Segundo apellido (si no tiene dejar en blanco)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu segundo apellido"}))
    phone = forms.CharField(max_length=32, label='Número de teléfono', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu numero de telefono"}))
    cp = forms.CharField(max_length=32, label='Código postal (Belle dluxe solo hace envios en tijuana)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu codigo postal"}))
    streat = forms.CharField(max_length=32, label='Calle (Belle dluxe solo hace envios en tijuana)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de tu calle"}))
    number_home = forms.CharField(max_length=32, label='Número exterior de casa (Belle dluxe solo hace envios en tijuana)', widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el numero exterior de tu casa"}))
    class Meta: 
        model= Customer
        fields = [ 
            "name_first",
            "last_name_pat",
            "last_name_mat",
            "phone",
            "cp",
            "streat",
            "number_home",
        ]
        """