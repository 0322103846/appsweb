from django.db import models

# Create your models here.

from django.urls import path, include
from home import views

app_name="home"
urlpatterns = [
   path('', views.home.as_view(),name='home') 
]