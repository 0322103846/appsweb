from django.urls import path
from core import views

app_name = "core"
urlpatterns = [
    path('estadios/',views.Estadio.as_view(), name="estadios"),
    path('create/estadio/',views.CreateEstadio.as_view(), name="create_estadio"),
    path('recover/estadio/<int:pk>/', views.DetailEstadio.as_view(), name= "recover_estadio"),
    path('update/estadio/<int:pk>/', views.UpdateEstadio.as_view(), name= "update_estadio"),
    path('delete/estadio/<int:pk>/', views.DeleteEstadio.as_view(), name= "delete_estadio"),
    
    path('equipos/',views.Equipo.as_view(), name="equipos"),
    path('create/equipo/',views.CreateEquipos.as_view(), name="create_equipo"),
    path('recover/equipo/<int:pk>/', views.DetailEquipos.as_view(), name= "recover_equipo"),
    path('update/equipo/<int:pk>/', views.UpdateEquipos.as_view(), name= "update_equipo"),
    path('delete/equipo/<int:pk>/', views.DeleteEquipos.as_view(), name= "delete_equipo"),

    path('ciudades/',views.Ciudad.as_view(), name="ciudades"),
    path('create/ciudad/',views.Createcuidad.as_view(), name="create_ciudad"),
    path('recover/ciudad/<int:pk>/', views.Detailciudad.as_view(), name= "recover_ciudad"),
    path('update/ciudad/<int:pk>/', views.Updateciudad.as_view(), name= "update_ciudad"),
    path('delete/ciudad/<int:pk>/', views.Deleteciudad.as_view(), name= "delete_ciudad"),
]