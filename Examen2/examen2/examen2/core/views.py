from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import *

# Create your views here.




##CRUD ESTADIOS

class Estadio(generic.View):
    template_name = "core/estadios/estadio.html"
    context = {}

    def get(self, request, *args, **kwargs):
        Estadio = Estadios.objects.all()
        self.context = {
            "Estadios": Estadio
        }
        return render(request, self.template_name,self.context)

class DetailEstadio(generic.DetailView):
    template_name = "core/estadios/recover_estadio.html"
    model = Estadios

class CreateEstadio(generic.CreateView):
    template_name = "core/estadios/create_estadio.html"
    model = Estadios
    form_class = NewEstadioForms
    success_url = reverse_lazy("core:estadios")

class UpdateEstadio(generic.UpdateView):
    template_name = "core/estadios/update_estadio.html"
    model = Estadios
    form_class = UpdateEstadioForms
    success_url = reverse_lazy("core:estadios")

class DeleteEstadio(generic.DeleteView):
    template_name = "core/estadios/delete_estadio.html"
    model = Estadios
    success_url = reverse_lazy("core:estadios")




##CRUD EQUIPOS

class Equipo(generic.View):
    template_name = "core/equipos/equipo.html"
    context = {}

    def get(self, request, *args, **kwargs):
        equipo =  Equipos.objects.all()
        self.context = {
            "Equipos": equipo
        }
        return render(request, self.template_name,self.context)
    
class CreateEquipos(generic.CreateView):
    template_name = "core/equipos/create_equipo.html"
    model = Equipos
    form_class = NewEquipoForms
    success_url = reverse_lazy("core:equipos")

class DetailEquipos(generic.DetailView):
    template_name = "core/equipos/recover_equipo.html"
    model = Equipos

class UpdateEquipos(generic.UpdateView):
    template_name = "core/equipos/update_equipo.html"
    model = Equipos
    form_class = UpdateEquipoForms
    success_url = reverse_lazy("core:equipos")

class DeleteEquipos(generic.DeleteView):
    template_name = "core/equipos/delete_equipo.html"
    model = Equipos
    success_url = reverse_lazy("core:equipos")



##CRUD CIUDADES

class Ciudad(generic.View):
    template_name = "core/ciudades/ciudad.html"
    context = {}

    def get(self, request, *args, **kwargs):
        Ciudad = Ciudades.objects.all()
        self.context = {
            "Ciudades": Ciudad
        }
        return render(request, self.template_name,self.context)

class Createcuidad(generic.CreateView):
    template_name = "core/ciudades/create_ciudad.html"
    model = Ciudades
    form_class = NewCiudadForms
    success_url = reverse_lazy("core:ciudades")

class Detailciudad(generic.DetailView):
    template_name = "core/ciudades/recover_ciudad.html"
    model = Ciudades

class Updateciudad(generic.UpdateView):
    template_name = "core/ciudades/update_ciudad.html"
    model = Ciudades
    form_class = UpdateCiudadForms
    success_url = reverse_lazy("core:ciudades")

class Deleteciudad(generic.DeleteView):
    template_name = "core/ciudades/delete_ciudad.html"
    model = Ciudades
    success_url = reverse_lazy("core:ciudades")
 