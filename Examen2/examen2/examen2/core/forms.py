from django import forms
from .models import *

##FORM PARA ESTADIOS

class NewEstadioForms(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Equipos": forms.Select(attrs={"class":"form-select form-control"}),
            }



class UpdateEstadioForms(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Equipos": forms.Select(attrs={"class":"form-select form-control"}),
            }

##FORMS PARA EQUIPOS

class NewEquipoForms(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Ciudad": forms.Select(attrs={"class":"form-select form-control"}),
            }
        
class UpdateEquipoForms(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Ciudad": forms.Select(attrs={"class":"form-select form-control"}),
            }

##FORMS PARA CIUDADES

class NewCiudadForms(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }

class UpdateCiudadForms(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }