from django.contrib import admin

# Register your models here.

from .models import *



@admin.register(campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor", 
        "name"
    ]

@admin.register(gender)
class GenderAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor", 
        "name"
    ]

@admin.register(category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor", 
        "name"
    ]

@admin.register(payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display=[
        "name", 
        "reference"
    ]

@admin.register(customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display=[
        "user", 
        "name_first", 
        "last_name_pat",
        "last_name_mat",
        "phone",
        "cp",
        "streat",
        "number_home"
    ]

@admin.register(order) 
class OrderAdmin(admin.ModelAdmin):
    list_display=[
        "payment", 
        "customer", 
        "date",
        "code_flow",
        "subtotal",
        "IVA",
        "total"
    ]

@admin.register(product)
class ProductAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor",
        "name",
        "imagen1",
        "imagen2",
        "imagen3",
        "campaign",
        "gender",
        "category",
        "description",
        "size",
        "price_uni",
        "status",
        "stock"
    ]
