from audioop import reverse
from django.db import models
from django.contrib.auth.models import User
 
# Create your models here.



class campaign(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=30, default='generico')
    year=models.DateField(auto_now_add=True)
    def __str__(self):
        return self.name

class gender(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=10, default='nonbinary')
    def __str__(self):
        return self.name

class category(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=25, default='generic caegory')
    def __str__(self):
        return self.name

class payment(models.Model):
    name=models.CharField(max_length=10)
    reference=models.CharField(max_length=8)
    def __str__(self):
        return self.name

class customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name_first=models.CharField(max_length=35)
    last_name_pat=models.CharField(max_length=25)
    last_name_mat=models.CharField(max_length=25, blank=True)
    phone=models.CharField(max_length=15)
    cp=models.CharField(max_length=5)
    streat=models.CharField(max_length=25)
    number_home=models.CharField(max_length=3)
    def __str__(self):
        return self.user


class order(models.Model):
    payment=models.ForeignKey(payment, on_delete=models.CASCADE)
    customer=models.ForeignKey(customer, on_delete=models.CASCADE)
    date=models.DateField(auto_now_add=True)
    code_flow=models.CharField(max_length=10)
    subtotal=models.FloatField(default=0.0)
    IVA=models.FloatField(default=0.0)
    total=models.FloatField(default=0.0)
    def __str__(self):
        return self.payment

class product(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=35)   
    campaign=models.ForeignKey(campaign, on_delete=models.CASCADE)
    gender=models.ForeignKey(gender, on_delete=models.CASCADE)
    category=models.ForeignKey(category, on_delete=models.CASCADE)
    description=models.CharField(max_length=256)
    size=models.CharField(max_length=4)
    price_uni=models.FloatField(default=0.0)
    status=models.BooleanField(default=False)
    stock=models.IntegerField(default=0)
    slug=models.SlugField(max_length=6)
    imagen1 = models.ImageField(upload_to='media/', null=True) 
    imagen2 = models.ImageField(upload_to='media/', null=True) 
    imagen3 = models.ImageField(upload_to='media/', null=True) 
    def __str__(self):
        return self.name

