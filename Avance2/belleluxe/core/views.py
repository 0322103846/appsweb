from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import *


class adm(generic.View):
    tamplate_name="core/admin.html"
    context={}
    def get(self, request, *args, **kwargs):
        return render(request, self.tamplate_name, self.context)
    



#Campaign--------------------------------------------------------------------------
class Campaign(generic.View):
   template_name="core/campaign/campaign.html"
   context={}
   def get(self,request):
       self.context= {
           "campaign": campaign.objects.all()
       }
       return render(request, self.template_name, self.context)
  
class DetailCampaign(generic.DetailView):
   template_name = "core/campaign/detail_campaign.html"
   model = campaign


class NewCampaign(generic.CreateView):
   template_name="core/campaign/NewCampaign.html"
   model = campaign
   form_class = NewCampaignform
   success_url = reverse_lazy("core:campaign")


class UpdateCampaign(generic.UpdateView):
   template_name="core/campaign/update_campaign.html"
   model = campaign
   form_class = UpdateCampaignForm
   success_url = reverse_lazy("core:campaign")


class DeleteCampaign(generic.DeleteView):
   template_name="core/campaign/delete_campaign.html"
   model = campaign
   success_url = reverse_lazy("core:campaign")


#Gender--------------------------------------------------------------------------
class list_gender(generic.View):
   template_name="core/gender/gender.html"
   context={}
   def get(self,request):
      genders = gender.objects.all()
      self.context = {
         "gender": genders
      }
      return render(request, self.template_name, self.context)

class NewGender(generic.CreateView):
   template_name="core/gender/New_gender.html"
   model = gender
   form_class = NewGenderForm
   success_url = reverse_lazy("core:gender")

class DetailGender(generic.DetailView):
   template_name = "core/gender/detail_gender.html"
   model = gender
   def get(self, request, pk, *args, **kwargs):
      genders = gender.objects.get(pk=pk)
      self.context = {
         "gender": genders
      }
      return render(request, self.template_name,self.context)

class UpdateGender(generic.UpdateView):
   template_name="core/gender/update_gender.html"
   model = gender
   form_class = UpdateGenderForm
   success_url = reverse_lazy("core:gender")

class DeleteGender(generic.DeleteView):
   template_name="core/gender/delete_gender.html"
   model = gender
   success_url = reverse_lazy("core:gender")

#Category--------------------------------------------------------------------------
class list_category(generic.View):
   template_name="core/category/category.html"
   context={}
   def get(self,request):
      categorys = category.objects.all()
      self.context = {
         "category": categorys
      }
      return render(request, self.template_name, self.context)

class NewCategory(generic.CreateView):
   template_name="core/category/New_category.html"
   model = category
   form_class = NewCategoryForm
   success_url = reverse_lazy("core:category")

class DetailCategory(generic.DetailView):
   template_name = "core/category/detail_category.html"
   model = category
   def get(self, request, pk, *args, **kwargs):
      categorys = category.objects.get(pk=pk)
      self.context = {
         "category": categorys
      }
      return render(request, self.template_name,self.context)

class UpdateCategory(generic.UpdateView):
   template_name="core/category/update_category.html"
   model = category
   form_class = UpdateCategoryForm
   success_url = reverse_lazy("core:category")

class DeleteCategory(generic.DeleteView):
   template_name="core/category/delete_category.html"
   model = category
   success_url = reverse_lazy("core:category")




#Payment--------------------------------------------------------------------------
class list_payment(generic.View):
   template_name="core/payment/payment.html"
   context={}
   def get(self,request):
      payments = payment.objects.all()
      self.context = {
         "payment": payments
      }
      return render(request, self.template_name, self.context)

class NewPayment(generic.CreateView):
   template_name="core/payment/New_payment.html"
   model = payment
   form_class = NewPaymentForm
   success_url = reverse_lazy("core:payment")

class DetailPayment(generic.DetailView):
   template_name = "core/payment/detail_payment.html"
   model = payment
   def get(self, request, pk, *args, **kwargs):
      payments = payment.objects.get(pk=pk)
      self.context = {
         "payment": payments
      }
      return render(request, self.template_name,self.context)

class UpdatePayment(generic.UpdateView):
   template_name="core/payment/update_payment.html"
   model = payment
   form_class = UpdatePaymentForm
   success_url = reverse_lazy("core:payment")

class DeletePayment(generic.DeleteView):
   template_name="core/payment/delete_payment.html"
   model = payment
   success_url = reverse_lazy("core:payment")


#Product--------------------------------------------------------------------------

class Product(generic.View):
   template_name = "core/product/product.html"
   context = {}
   def get(self, request, *args, **kwargs):
      products = product.objects.all()
      self.context = {
         "products": products
      }
      return render(request, self.template_name, self.context)

class NewProduct(generic.CreateView):
   template_name = "core/product/new_product.html"
   model = product
   form_class = NewProductForm
   success_url = reverse_lazy("core:product")
class RecoverProduct(generic.View):
   template_name = "core/product/recover_product.html"
   context = {}

   def get(self, request, pk, *args, **kwargs):
      self.context = {
         "product": product.objects.get(pk=pk)
      }
      return render(request, self.template_name,self.context)

class UpdateProduct(generic.UpdateView):
   template_name = "core/product/update_product.html"
   model = product
   form_class = UpdateProductForm
   success_url = reverse_lazy("core:product")

class DeleteProduct(generic.DeleteView):
   template_name = "core/product/delete_product.html"
   model = product
   success_url = reverse_lazy("core:product")


#Customer-----------------------------------------------------------------------
"""
class list_customer(generic.View):
   template_name="core/customer/customer.html"
   context={}
   def get(self,request):
       self.context= {
           "customer": customer.objects.all()
       }
       return render(request, self.template_name, self.context)
  
class Detailcustomer(generic.DetailView):
   template_name = "core/customer/detail_customer.html"
   model = customer


class Newcustomer(generic.CreateView):
   template_name="core/customer/NewCustomer.html"
   model = customer
   form_class = NewCustomerForm
   success_url = reverse_lazy("core:customer")


class Updatecustomer(generic.UpdateView):
   template_name="core/customer/update_customer.html"
   model = customer
   form_class = UpdateCustomerForm
   success_url = reverse_lazy("core:customer")


class Deletecustomer(generic.DeleteView):
   template_name="core/customer/delete_customer.html"
   model = customer
   success_url = reverse_lazy("core:customer")

#Order--------------------------------------------------------------------------

class list_order(generic.View):
   template_name="core/order/order.html"
   context={}
   def get(self,request):
       self.context= {
           "order": order.objects.all()
       }
       return render(request, self.template_name, self.context)
  
class Detailorder(generic.DetailView):
   template_name = "core/order/detail_order.html"
   model = order


class Neworder(generic.CreateView):
   template_name="core/order/NewOrder.html"
   model = order
   form_class = NewOrderForm
   success_url = reverse_lazy("core:order")


class Updateorder(generic.UpdateView):
   template_name="core/order/update_order.html"
   model = order
   form_class = UpdateOrderForm
   success_url = reverse_lazy("core:gender")


class Deleteorder(generic.DeleteView):
   template_name="core/order/delete_order.html"
   model = order
   success_url = reverse_lazy("core:order")
"""