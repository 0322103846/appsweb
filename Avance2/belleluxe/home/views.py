from django.shortcuts import render
from django.views import generic
# Create your views here.
class inicio(generic.View):
    tamplate_name="home/inicio.html"
    context={} 
    def get(self, request):
        return render(request, self.tamplate_name, self.context)
    
class nosotros(generic.View):
    tamplate_name="home/nosotros.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)
    
class blog(generic.View):
    tamplate_name="home/blog.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)
    
class servicios(generic.View):
    tamplate_name="home/servicios.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)

class politicas(generic.View):
    tamplate_name="home/politicas.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)
    
class carrito(generic.View):
    tamplate_name="home/carrito.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)